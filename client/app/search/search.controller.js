(function (){
    angular
        .module("MyApp")
        .controller("SearchC", SearchC);
    
    SearchC.$inject = ['GService', '$state'];

    function SearchC(GService, $state) {
        var con = this;
        con.searchDB = searchDB;
        con.searchAnd = searchAnd;
        con.searchOr = searchOr;
        con.produceImg = produceImg;

        // init
        function initialize() {
            con.name = "";
            con.brand = "";
            con.searchby = "";
            con.searched = false;
            con.groceries = [];
        }
        initialize();

        // searchDB 
        // only when results then con.searched = true;

        function produceImg(upc12) {
            result = "http://www.barcodes4.me/barcode/c128a/" + upc12 + ".png?IsTextDrawn=1"
            return result
        }

        function searchAnd() {
            con.searchby = "and";
            searchDB();
            // TO DO: 
            // only toggle if both fields are NOT empty 
        }

        function searchOr() {
            con.searchby = "";
            searchDB();
        }

        function searchDB() {
            var params = {
                "name": con.name,
                "brand": con.brand,
                "searchby": con.searchby,
            };
            // need to reset

            GService
                .retrieveDB(params) // note returns an array of JSON
                .then(function(result){
                    console.log("Expect this to be array of JSON>>> " + JSON.stringify(result.data))
                    con.searched = true;
                    con.groceries = result.data;
                })
                .catch(function (err) {
                    console.log("error " + err);
                });
        }// end searchDB 
    
        function editItem(id) {
            console.log(id);
            $state
                .go('editItem', {
                    id: id
                });
        }


    }; // end controller

})();