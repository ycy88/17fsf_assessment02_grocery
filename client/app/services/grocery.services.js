(function (){
    angular
        .module("MyApp")
        .service("GService", GService);
    
    GService.$inject = ["$http"];

    function GService($http) {
        var service = this;

        service.retrieveDB = retrieveDB;
        service.retrieveOne = retrieveOne;
        service.updateOne = updateOne;

        function retrieveDB(params) {
            return $http.get("/api/groceries", {
                params: params
            }
            );
        }

        function retrieveOne(id) {
            return $http({
                method: 'GET',
                url: "api/groceries/" + id
            });
        }

        function updateOne(id, params) {
            return $http({
                method: 'PUT', 
                url: "api/groceries/" + id,
                data: params  
            })
        }

    }// end GService 
    
})();