(function(){
    // add the .config property which holds uiRouteConfig
    angular
        .module("MyApp")
        .config(uiRouteConfig)
    
    // attach $inject property 
    uiRouteConfig.$inject = ["$stateProvider", "$urlRouterProvider"];

    // uiRouteConfig 
    function uiRouteConfig($stateProvider, $urlRouterProvider){
        // .state takes 2 params - state name string & object
        // can chain
        // optional to define controller here

        // create one state first
        $stateProvider
            .state('search',{
                url : '/search',
                templateUrl: '/app/search/search.html',
                controller: 'SearchC as con',
                // controller: 'SearchC as con'
            }
            )
            .state('editItem', {
                url : '/edit/:id',
                templateUrl: '/app/edit/edit.html',
                controller: 'EditC',
                controllerAs: 'con'
                // 'EditC as con'
            }
            );
        
        // set catchall URL
        $urlRouterProvider.otherwise('/search');

    }// close uiRouteConfig 


})();