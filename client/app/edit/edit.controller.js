(function(){
    angular
        .module("MyApp")
        .controller("EditC", EditC);
    
    EditC.$inject = ['GService', '$stateParams'];

    function EditC(GService, $stateParams) {
        var con = this;
        
    
        // can assume $stateParams defined by default, no else 
        // since the only route here is from search
        // $stateParams = {"id":30874,"upc12":41387221503,"brand":"4c","name":"4c Totally Light Sugar Free Tea 2go Red Tea Antioxidant Peach Drink Mix - 20 Ct"}

        // init
        con.id = "";
        con.brand = "";
        con.upc12 = "";
        con.name = "";
        con.editItem = editItem;

        if ($stateParams) {
            con.id = $stateParams.id;
            showItem();
        }

        function editItem() {
            var bodyParams = {
                name: con.name,
                brand: con.brand,
                upc12: con.upc12
            }

            console.log("The id is >>>>" + con.id);
            GService
                .updateOne(con.id, bodyParams)
                .then(function(result){
                    console.log("The records changed is>>>" + JSON.stringify(result));
                    alert("You have updated the record to:" + JSON.stringify(result.config.data));
                })
                .catch(function(err){
                    console.log("Error is>>> " + err);
                });

        }


        // note that con.id is called only when the search executes 
        // result.data {"id":8093,"upc12":70650800091,"brand":"A Taste Of Thai","name":"A Taste Of Thai Red Curry Paste"}
        function showItem() {
            GService
                .retrieveOne(con.id)
                .then(function(result){
                    console.log(JSON.stringify(result.data));
                    if (!result.data) return;

                    con.id = result.data.id;
                    con.brand = result.data.brand;
                    con.upc12 = result.data.upc12;
                    con.name = result.data.name;
                })
                .catch(function(err){
                    console.log("The error is >>>" + err);
                });
        }// showItem
        

    };



})();