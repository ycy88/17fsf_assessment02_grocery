// mysql username and pw all root root
var config = require('./config');

var Sequelize = require("sequelize");

// small sequelize is the DB object, first param is DB name: grocery 
var sequelize = new Sequelize('grocery',
    config.MYSQL_USERNAME,
	config.MYSQL_PASSWORD,
    {
		host: config.MYSQL_HOSTNAME,
		port: config.MYSQL_PORT,
		logging: config.MYSQL_LOGGING,
		dialect: 'mysql',
		pool: {
			max: 5,
			min: 0,
			idle: 10000,
		},
	}
);

// use .import to avoid double loading problem with require 
// note Grocery model == grocery_list table
const Grocery = sequelize.import('./models/grocery'); 

// export to app.js 
module.exports = {
    Grocery: Grocery
};