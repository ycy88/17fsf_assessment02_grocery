// define groceryList model
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('grocery', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
    },
    upc12: {
      type: DataTypes.BIGINT(11),
      allowNull: false,
    },
    brand: {
      type: DataTypes.STRING,
      allowNull: false,
    },  
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  }, {
    timestamps: false, // no timestamps
    tableName: 'grocery_list', // to fit .sql table name
  });
};
