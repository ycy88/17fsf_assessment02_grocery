const express = require('express');
const path = require("path");
const bodyParser = require("body-parser");

// sequelize if any
////////

// config settings - require config file before calling
const config = require('./config');
const NODE_PORT = process.env.NODE_PORT || config.PORT || 3000;

// routes
const CLIENT_FOLDER = path.join(__dirname, '/../client');
const MSG_FOLDER = path.join(CLIENT_FOLDER, '/assets/messages');

// express 
var app = express();

// load db 
const db = require('./db.js');

// routes
// first catch == client folder
app.use(express.static(CLIENT_FOLDER));

// second catch form data, using bodyParser
app.use(bodyParser.json());

// api routes
require('./routes.js')(app, db);

// error catchalls
// 404
app.use(function(req,res){
    res.status(404).sendFile(path.join(MSG_FOLDER + "/404.html"));
});

// 500
app.use(function(err, req, res, next) {
	res.status(500).sendFile(path.join(MSG_FOLDER + '/500.html'));
});

console.log("Port is now >>>>" + NODE_PORT);

app.listen(NODE_PORT, function(){
    console.log("Server now running on localhost: %d", NODE_PORT);
});