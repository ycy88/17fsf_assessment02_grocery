
// db is passed in when executed
// no need create 

// var create = function(db) {
//   return function(req, res) {
//     console.log('This is the data... >>>>\n');
//     console.log('>>>>' + JSON.stringify(req.body));

//     db.Grocery
//       .create({
//         id: req.body.grocery.id,
//         upc12: req.body.grocery.upc12,
//         brand: req.body.grocery.brand,
//         name: req.body.grocery.name,
//       })
//       .then(function(grocery) {
//         console.log('Success');
//         res
//           .status(200)
//           .json(grocery);
//       })
//       .catch(function(err) {
//         console.log("error: " + err);
//         res
//           .status(500)
//           .json(err);
//       }); // end return
//   }
// };// create 



// SHOW //////


var show = function(db){
    return function(req, res){
        console.log('This is the data>>' + JSON.stringify(req.params));
        // we get it via req,params
        if (req.params.id) {
            var where = { id: req.params.id }
        } else {
            where = {}; // returns error
        }
        db.Grocery
            .findOne({ where: where})
            .then(function (result) {
                console.log("Result >>>>>" + JSON.stringify(result));
                res.status(200).json(result);
            })
            .catch(function (err) {
                console.log(err);
                res.status(500).json({error: true});
            });
    }
}



// RETRIEVE ////

// we receive the searchString via a GET request
// data is found in query string (req.query)
var retrieve = function(db) {
    return function(req, res) {
        console.log('This is the data... >>>>\n');
        console.log('>>>>' + JSON.stringify(req.query));
    
        // search params 
        // if empty string, set search value to rubbish string for null set
        // empty string is falsey in js, remain rubbish
        // refactor if time permits to ease db load 

        var brand = "FJHJFHFJHF";
        var name = "FKJFKHFHFH";
        if (req.query.name) {
            name = "%" + req.query.name + "%";
        }
        if (req.query.brand) {
            brand = "%" + req.query.brand + "%";
        }

        var searchby = "$or"
        if (req.query.searchby) {
            searchby = "$" + req.query.searchby;
        }
        // ES6 computed object property to set name for searchby= AND
        var where = {
            [searchby]: [
                { brand: { $like: brand } },
                { name: { $like: name } }
            ]
        }
        console.log("Searcby is>>>" + searchby)

        db.Grocery
            .findAll({
                where: where,
                limit: 20,
                order: ['name'], 
            }
            )// returns an array of JSON
            //  raw query not working...
            // .query('SELECT * FROM groceries_list WHERE name LIKE ?',
            //     { model: 'grocery' },
            //     { replacements: [query], type: sequelize.QueryTypes.SELECT }
            // )
            .then(function(grocery) {
                console.log("Results >>> " + JSON.stringify(grocery));
                res.status(200).json(grocery);
            })
            .catch(function(err) {
                console.log("Grocery error clause: " + err);
                res.status(500).json(err);
            });
    }// end return
};// end retrive


var edit = function(db) {
    return function(req, res) {
        console.log('This is the id in URL params>>>>' + JSON.stringify(req.params));
        console.log('This is the field to be changed>>>>' + JSON.stringify(req.body));
    // find record sent by URL params 
    // change field sent by req.body
    // note all params are passed in and pre-populated

    db.Grocery
        .update({
            name: req.body.name,
            brand: req.body.brand,
            upc12: req.body.upc12
        }, {
            where : {
                id: req.params.id
            }
        }
        )
        .then(function(grocery) {
            res.status(200).json(grocery);
            })
        .catch(function(err) {
            console.log("Employee error clause: " + err);
            res.status(500).json(err);
        });
    }// end return callback
}; // end edit 

module.exports = function(db) {
    return {
        retrieve: retrieve(db),
        edit: edit(db),
        show: show(db),
    }
};

