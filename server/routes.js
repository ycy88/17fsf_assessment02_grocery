// db and app defined in app.js
// pass db to Grocery controller, so we can call db.model 

module.exports = function(app, db) {
    // import the controller files for Models which methods u wanna call
    var Grocery = require('./api/grocery.controller.js')(db);

    // callback Grocery methods
    // retrieveMany
    app.get("/api/groceries", Grocery.retrieve);

    // edit
    app.put('/api/groceries/:id', Grocery.edit);

    // show
    app.get('/api/groceries/:id', Grocery.show);

}

